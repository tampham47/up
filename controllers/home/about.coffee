'use strict'

angular.module('student-fly.home')
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/about',
		controller: 'about-ctrl'
		templateUrl: 'views/home/about.jade'

.controller 'about-ctrl', ($scope, $location) ->
	$scope.title = 'about-ctrl'
