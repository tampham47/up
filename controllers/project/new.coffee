'use strict'

angular.module('student-fly.project')
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/project/new',
		controller: 'project-new-ctrl'
		templateUrl: 'views/project/new.jade'

.controller 'project-new-ctrl', ($scope, $location) ->
	$scope.title = 'project-new-ctrl'
