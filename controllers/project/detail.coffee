'use strict'

angular.module('student-fly.project')
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/project/detail',
		controller: 'project-detail-ctrl'
		templateUrl: 'views/project/detail.jade'

.controller 'project-detail-ctrl', ($scope, $location) ->
	$scope.title = 'project-detail-ctrl'
	$scope.ctrlHolder = 'views/project/introduction.jade'

	$scope.nav = (theme) ->
		$scope.ctrlHolder = 'views/project/' + theme
