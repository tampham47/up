'use strict'

angular.module('student-fly.project', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/',
		controller: 'project-ctrl'
		templateUrl: 'views/project/index.jade'
.config ($routeProvider) ->
	$routeProvider.when '/project',
		controller: 'project-ctrl'
		templateUrl: 'views/project/index.jade'

.controller 'project-ctrl', ($scope, $location) ->
	$scope.title = 'Dự án'
