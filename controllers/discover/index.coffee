'use strict'

angular.module('student-fly.discover', [])
# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/discover',
		controller: 'discover-ctrl'
		templateUrl: 'views/discover/index.jade'

.controller 'discover-ctrl', ($scope, $location) ->
	$scope.title = 'discover'
