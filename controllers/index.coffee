'use strict'

# require order is important
angular.module 'the-up', [
	'ngAnimate'
	'ngRoute'

	'student-fly.templates'
	'student-fly.home'
	'student-fly.project'
	'student-fly.discover'
]
