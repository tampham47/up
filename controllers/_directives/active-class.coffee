'use strict'

angular.module('the-up')
.directive 'ngHref', ($location) ->
	link: (scope, element, attrs) ->
		$ ->
			element.attr 'href', attrs.ngHref

			$(element).on 'click', ->
				$('li', $(this).parent().parent()).removeClass 'active'
				$(this).parent().addClass 'active'

				# other case
				$('a', $(this).parent().parent()).removeClass 'active'
				$(this).addClass 'active'
