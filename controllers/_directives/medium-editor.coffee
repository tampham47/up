'use strict'

angular.module('the-up')
.directive "ngMeditor", ->
	restrict: "A"
	require: "ngModel"
	link: (scope, element, attrs, ngModelCtrl) ->
		$ ->
			# medium editor
			editor = new MediumEditor($(element))

			# medium editor insert plugin
			# $(element).mediumInsert
			# 	editor: editor
			# 	images: true
			# 	imagesUploadScript: '/upload/post'

			$(element).on 'input', ->
				ngModelCtrl.$setViewValue $(this).html()
